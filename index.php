<?php

function browserify($string) {
    return '<p>'.
        str_replace(
        ' ',
        ' ',
            str_replace(
            "\t",
            '    ',
                str_replace(
                "\n",
                '<br>',
                    str_replace(
                    "\r",
                    '<br>',
                    str_replace(
                        "\r\n", 
                        '<br>',
                        $string
                    )
                    )
                )
            )
        )
        .'</p>';
            
}


function multiline_command($cmd){
    $return = [];
    exec($cmd, $return);
    return implode($return, '<br>').'<br>';    
}


function parse_leases(){
    $active_pre = explode("\n", file_get_contents('/var/lib/dnsmasq/dnsmasq.leases'));
    $configured_pre = explode("\n", file_get_contents('/etc/dnsmasq.d/leases.conf'));
    $active = new stdClass;
    $configured = new stdClass;
    foreach($active_pre as $value){
        list($timestamp, $mac, $ip, $clientname, $mac_with_arpid) = explode(" ", $value);
        
        $active->{$mac} = [
            'time' => $timestamp,
            'mac' => $mac,
            'name' => $clientname,
            'ip' => $ip,
        ];
    }
    
    foreach($configured_pre as $value){

        list($host, $clientname, $ip) = explode(",", $value);
        $mac = str_replace('dhcp-host=', '', $host);
        
        $configured->{$mac} = [
            'mac' => $mac,
            'name' => $clientname,
            'ip' => $ip,
        ];
    }

    $final = new stdClass;

    foreach (array_values((array)$configured) as $conf){
        $final->{$conf['mac']} = [
            'active' => false,
            'expires_in' => 0,
            'configured' => true,
            'mac' => $conf['mac'], 
            'name' =>  $conf['name'],
            'dhcp_name' =>  $conf['name'],
            'ip' =>  $conf['ip'],
            'dhcp_ip' =>  $conf['ip']
        ];    
    }

    foreach (array_values((array)$active) as $act){
        if(property_exists($final, $act['mac'])){
            $seen = new DateTime();
            $seen->setTimezone(new DateTimezone('Europe/Berlin'));
            $seen->setTimestamp((int)$act['time']);
            $final->{$act['mac']}['active'] = true;
            $final->{$act['mac']}['expires_in'] = $seen->format('d.m.Y H:i:s') . ', in ' . ($seen->getTimestamp() - (new DateTime)->getTimestamp()) . 's';
            $final->{$act['mac']}['dhcp_name'] = $act['name'];
            $final->{$act['mac']}['dhcp_ip'] = $act['ip'];
        } else {
            $final->{$act['mac']} = [
                'active' => true,
                'expires_in' => $act['time'],
                'configured' => false,
                'mac' => $act['mac'], 
                'name' =>  $act['name'],
                'dhcp_name' =>  $act['name'],
                'ip' =>  $act['ip'],
                'dhcp_ip' =>  $act['ip']
            ];    
        }
    }

    $table = '<table><tr>'
        .'<th>Status</th>'
        .'<th>MAC</th>'
        .'<th>IP</th>'
        .'<th>NAME</th>'
        .'<th>Configured</th>'
        .'<th>Active</th>'
        .'<th>DHCP Expires at</th>'
        .'</tr>';
    foreach(array_values((array) $final) as $entry){
        $classes= 'what';
        $had_attr = false;

        if($entry['ip'] === $entry['dhcp_ip'] && ($entry['name'] === $entry['dhcp_name'] || $entry['dhcp_name'] === NULL) ){
            if($entry['active']){
                $classes = ' active ';
                $had_attr = true;
            }
            if($entry['configured']){
                if($had_attr){
                $classes .= ' configured ';
                } else {
                    $classes = ' configured ';
                }
            }
        }
    
        if($entry['mac'] !== ''){
            $table .= '<tr class="' . $classes .'">'
            .'<td class="'.$classes.'">'.'</td>'
            .'<td>'.$entry['mac'].'</td>'
            .'<td>'.($entry['ip'] === $entry['dhcp_ip'] ? $entry['ip'] : '<span style="color:red">DHCP: '.$entry['dhcp_ip'] .' Configured: '.$entry['ip'].'</span>').'</td>'
            .'<td>'.($entry['name'] === $entry['dhcp_name'] || $entry['dhcp_name'] === NULL ? $entry['name'] : '<span style="color:red">DHCP: '.$entry['dhcp_name'] .' Configured: '.$entry['name'].'</span>').'</td>'
            .'<td '.($entry['configured'] ? 'style="color: green">✓' : 'style="color: red">✗').'</td>'
            .'<td '.($entry['active'] ? 'style="color: green">✓' : 'style="color: red">✗').'</td>'
            .'<td '.($entry['expires_in'] !== '0' && $entry['expires_in'] !== 0  ? '>'.$entry['expires_in'] : 'style="color: red">✗').'</td>'
            .'</tr>';
        }
    }
    return $table.'</table>';
}


if(array_key_exists('refresh', $_GET) && (int)$_GET['refresh'] >= 0){
    header('Refresh:'.(int)$_GET['refresh']);
}




$content = '<h1>Router Status</h1>';
$content .=  '<h2>Leases</h2>';
$content .= parse_leases();
$content .= '<h2>Nft Ruleset</h2>';
$content .= '<p>'.browserify(multiline_command('nft list ruleset')).'</p>';
$content .= '<h2>SS</h2>';
$content .= '<p>'.browserify(multiline_command('ss -stunap')).'</p>';
//Omit: x (unix)
$content .= '<h2>SS FULL</h2>';
$content .= '<p>'.browserify(multiline_command('ss -OalempiZz460tudwsr --tos --vsock --xdp')).'</p>';


$out = str_replace('{{CONTENT}}', $content, file_get_contents(__DIR__.'/content/base.html'));

echo($out);


